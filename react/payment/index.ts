import type {
  PaymentParams,
  RefundParams,
  ResponsePaymentData,
  ResponseRefundData,
} from '../params'
import { PaymentError } from '../params'
import { logMsg, logError, formatPrice, logGeneralInfo } from '../utils/log'

declare global {
  interface Window {
    paymentSuccess: VoidFunction
    paymentFail: VoidFunction
    refundSuccess: VoidFunction
    refundFail: VoidFunction
    popupClose: VoidFunction
  }
}

const TRX_URI = 'https://transactions.vtexnorthlatam.com'

function getExampleOfPaymentSuccess(
  params: PaymentParams
): ResponsePaymentData {
  return {
    paymentId: params.paymentId,
    cardBrandName: 'VISA',
    firstDigits: '0123',
    lastDigits: '456789',
    acquirerName: 'my acquirer name',
    tid: '1234',
    acquirerAuthorizationCode: '1234',
    nsu: '1234',
    merchantReceipt: 'Card payment receipt to merchant',
    customerReceipt: 'Card payment receipt to customer',
    responsecode: 0,
    reason: '',
  }
}

function getExampleOfRefundSuccess(params: RefundParams): ResponseRefundData {
  return {
    paymentId: params.paymentId,
    paymentAcquirerAuthorizationCode: '1234',
    acquirerAuthorizationCode: '1234',
    merchantReceipt: 'Card refund receipt to merchant',
    customerReceipt: 'Card refund receipt to customer',
    responsecode: 0,
    reason: '',
  }
}

/**
 * TRANSACTION
 */
async function makeTransaction(data, deviceID, taxRate) {
  let response = [] as any

  const dataTransaction = {
    orderId: data.orderGroupId,
    paymentId: data.paymentId,
    transactionId: data.transactionId,
    deviceId: deviceID._id.toString(),
    paymentType: 'webservice-redeban-pay',
    payerEmail: data.payerEmail,
    payerIdentification: data.payerIdentificatio
      ? data.payerIdentification
      : '1111',
    amount: data.amount / 100,
    taxRate: taxRate / 100,
    debugMode: true,
  }

  try {
    const transaction = await fetch(TRX_URI + '/api/transactions', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(dataTransaction),
    })
    response = await transaction.json()

    if (response.data._id) {
      const wsid = response.data._id
      sendInfo(
        '<img src="https://newbusinessunits.vteximg.com.br/arquivos/ids/155521/pos_loading.gif" alt="pos_loading" width="100">',
        'container-loader'
      )

      sendInfo(
        'El datáfono está listo para iniciar la transacción',
        'process-info'
      )

      const ms = new WebSocket(
        'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + wsid
      )
      ms.addEventListener('message', function (event) {
        const responseData = JSON.parse(event.data)
        const transaction = getTransactionData(responseData, data)
        console.log('-> transaction', transaction)
        ms.close()
        window.paymentSuccess()
        window.location.reload()
      })
    } else {
      sendInfo('No se pudo encontrar el ID, data.id - null', 'process-info')
    }
  } catch (e) {
    sendInfo('No se pudo crear la orden' + e.message, 'process-info')
    console.error('Error', e.message)
  }
}

function getTransactionData(responseData, trx) {
  const di = getDigits(responseData.paymentResponse.response.cardNumbers)
  return {
    scheme: 'instore',
    action: 'payment',
    acquirerProtocol: trx.acquirerProtocol,
    paymentId: trx.paymentId,
    cardBrandName: responseData.paymentResponse.response.franchise,
    acquirerName: responseData.paymentResponse.response.accountType,
    firstDigits: di.first,
    lastDigits: di.last,
    tid: responseData.paymentRequestId,
    acquirerAuthorizationCode: responseData.paymentResponse.response.paymentID,
    nsu: responseData._id,
    merchantReceipt: responseData.paymentResponse.response.payeeTransactionId,
    customerReceipt: responseData.paymentResponse.response.approveCode,
    responsecode: getResponseCode(responseData.status),
    success: getSuccess(getResponseCode(responseData.status)),
    reason: !getSuccess(getResponseCode(responseData.status))
      ? 'Pago rechazado en el dispositivo'
      : null,
  }
}

function getDigits(data) {
  if (data !== '') {
    const digits = data.split('**')
    return {
      first: digits[0],
      last: digits[1],
    }
  }
  return {
    first: '',
    last: '',
  }
}

function getResponseCode(status) {
  if (status === 'approved') {
    return 0
  } else {
    return 1
  }
}

function getSuccess(status) {
  return status === 0
}

async function getDeviceID(swl) {
  const trResponse = await fetch(TRX_URI + '/devices/listbyname/' + swl)
  const deviceID = await trResponse.json()
  console.log('getDeviceID -> deviceID', deviceID)
  return deviceID[0]
}

async function getTaxRate(sellerName) {
  const getTax = await fetch(TRX_URI + '/settings/' + sellerName)
  const tax = await getTax.json()
  return tax.data.taxRate
}

async function sendInfo(htmlContent, classInfo) {
  console.log('sendInfo -> htmlContent', htmlContent)
  const elem = document.querySelector('.' + classInfo)

  if (!elem) {
    throw new PaymentError('No root container found')
  }
  elem.innerHTML = ''
  elem.innerHTML += htmlContent
}

export async function initPayment(
  params: PaymentParams
): Promise<ResponsePaymentData> {
  console.log('params', params)
  const result = document.querySelector('#popup-container')
  if (result) {
    result.innerHTML = ''
  }

  logGeneralInfo(
    params.paymentType === 'debit' ? 'Debit' : 'Credit',
    '#payment-type'
  )

  logGeneralInfo(params.paymentId, '#payment-id')
  logGeneralInfo(formatPrice(params.amount / 100), '#amount')

  if (params.swl) {
    console.log('params.swl', params.swl)
    getDeviceID(params.swl)
      .then((deviceID) => {
        getTaxRate(params.sellerName).then((taxRate) => {
          makeTransaction(params, deviceID, taxRate)
        })
      })
      .catch((err) => {
        sendInfo(
          'No se pudo obtener los datos del despositivo' + err.message,
          'process-info'
        )
      })

    const rootContainer = document.querySelector('#popup-container')

    if (!rootContainer) {
      throw new PaymentError('No root container found')
    }

    rootContainer.innerHTML += `
        <div id="nbCustom" class="payment-popup">
          <div class="absolute top-0 right-0 pa3">
            <button id="success" onclick="popupClose()">
              CLOSE
            </button>
          </div>
          <div class="flex flex-column items-center justify-center popup-content-center">
            <div class="flex bg-white h3 pv10 items-center  justify-center w-50 container-loader">
              <div class="loader"></div>
            </div>
            <div class="bg-white h3 w-50 tc">
              <h4 class="process-info">Obteniendo transacción, por favor espere</h4>
            </div>
            <div class="flex bg-white h3 ph3 items-center center justify-center mb0 w-50">
              <button id="fail" onclick="paymentFail()" class="general-btn btn-cancel">
                Cancelar pago
              </button>
            </div>
          </div>
        </div>
      `
  } else {
    alert('No se pudo encontrar los datos de la tienda. Franquicia - null')
  }

  window.popupClose = () => {
    const elem = document.querySelector('.payment-popup')

    if (elem) {
      elem.parentNode?.removeChild(elem)
    }
  }

  return new Promise((res, rej) => {
    window.paymentSuccess = () => {
      const response = getExampleOfPaymentSuccess(params)

      logMsg(`Payment finished with response: ${JSON.stringify(response)}`)

      res(response)
    }

    window.paymentFail = () => {
      logError('Payment finished with an error')

      rej(new PaymentError('An error happened during payment'))
    }
  })
}

export async function initRefund(
  params: RefundParams
): Promise<ResponseRefundData> {
  logMsg(`Refund starting for payment id: ${params.paymentId}`)

  const rootContainer = document.querySelector('#popup-container')

  if (!rootContainer) {
    throw new PaymentError('No root container found')
  }

  rootContainer.innerHTML += `
    <div class="payment-popup">
      <div class="absolute top-0 right-0 pa3">
        <button id="success" onclick="popupClose()">
          CLOSE
        </button>
      </div>
      <div class="flex flex-column items-center justify-center popup-content-center">
        <p class="flex bg-white h3 ph3 items-center center justify-center mb0 w-50">
          Choose option to test inStore response
        </p>
        <div class="flex bg-white h3 ph3 items-center center justify-center mb0 w-50">
          <button id="success" onclick="refundSuccess()">
            REFUND SUCCESS
          </button>
          <button id="fail" onclick="refundFail()">
          REFUND FAIL
          </button>
        </div>
      </div>
    </div>
  `

  window.popupClose = () => {
    const elem = document.querySelector('.payment-popup')

    if (elem) {
      elem.parentNode?.removeChild(elem)
    }
  }

  return new Promise((res, rej) => {
    window.refundSuccess = () => {
      const response = getExampleOfRefundSuccess(params)

      logMsg(`Refund finished with response: ${JSON.stringify(response)}`)

      res(getExampleOfRefundSuccess(params))
    }

    window.refundFail = () => {
      logError('Refund finished with an error')

      rej(new PaymentError('An error happened during refund'))
    }
  })
}
